**StackML Assignment**

Job Application: Full Stack Developer

A basic flask app allowing you to upload an image and review all the 3 levels specified within the assignment.


* [x]  Level 1:
If the uploaded image contains any human face censor it with an emoji. 


* [x]  Level 2:
If the uploaded image contains multiple human faces, provide an option to the user to select faces to censor or not.


* [x]  Level 3:
In the uploaded image recognize emotion for each face & use respective emoji (happy/sad/angry etc.) to censor the faces.


**Software requirements**:
Python3,
Git

**Clone the repository**
```
git clone https://gitlab.com/rootbid/application-stackml.git
cd application-stackml
```

**Commands to get the app running on a windows system**,
```
source Scripts/activate
python app/main.py  # In your browser visit http://localhost:5010/
```

**Commands to get the app running on a linux system**,
```
python3 -m venv .  # apt-get install python3-venv, to install venv
source bin/activate
pip install -r requirements.txt
python app/main.py  # In your browser visit http://localhost:5010/
```


*NOTE*:
1. Secret and Access keys have been commited, to enable easy accessibility. Under the impression that these do not relate to any sensitive information loss.
2. Locate a few images to test with, at application-stackml/app/static/images/test (If testing on localhost)
