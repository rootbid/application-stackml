import os
from flask import Flask, escape, request, redirect, url_for, send_from_directory, render_template, flash
from werkzeug.utils import secure_filename
from flask_images import Images, resized_img_src

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
UPLOAD_FOLDER_WIN64_x86 = os.path.join(BASE_DIR, 'app\\static\\images\\uploads')
UPLOAD_FOLDER_UNIX = os.path.join(BASE_DIR, 'app/static/images/uploads')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)
app.secret_key = "crazy_assignment"
images = Images(app)
app.config['UPLOAD_FOLDER_WIN64_x86'] = UPLOAD_FOLDER_WIN64_x86
app.config['UPLOAD_FOLDER_UNIX'] = UPLOAD_FOLDER_UNIX
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
# app.config['IMAGES_PATH'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # Check if post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            try:
                filepath = os.path.join(app.config['UPLOAD_FOLDER_WIN64_x86'], filename)
                file.save(filepath)
            except FileNotFoundError:
                filepath = os.path.join(app.config['UPLOAD_FOLDER_UNIX'], filename)
                file.save(filepath)
            if request.form['levels'] == "level_1":
                return redirect(url_for(
                    'level_1',
                    filename=filename))
            elif request.form['levels'] == "level_2":
                return redirect(url_for(
                    'level_2',
                    filename=filename))
            else:
                return redirect(url_for(
                    'level_3',
                    filename=filename))
    return render_template('upload.html')

@app.route('/level1/<filename>')
def level_1(filename):
    try:
        filepath = os.path.join(app.config['UPLOAD_FOLDER_WIN64_x86'], filename)
    except FileNotFoundError:
        filepath = os.path.join(app.config['UPLOAD_FOLDER_UNIX'], filename)
    return render_template('level1.html', file=filename)

@app.route('/level2/<filename>')
def level_2(filename):
    try:
        filepath = os.path.join(app.config['UPLOAD_FOLDER_WIN64_x86'], filename)
    except FileNotFoundError:
        filepath = os.path.join(app.config['UPLOAD_FOLDER_UNIX'], filename)
    return render_template('level2.html', file=filename)

@app.route('/level3/<filename>')
def level_3(filename):
    try:
        filepath = os.path.join(app.config['UPLOAD_FOLDER_WIN64_x86'], filename)
    except FileNotFoundError:
        filepath = os.path.join(app.config['UPLOAD_FOLDER_UNIX'], filename)
    return render_template('level3.html', file=filename)


if __name__ == '__main__':
    app.run(debug=False, use_reloader=True, port=5010, host='0.0.0.0')